module counter3 (Clk, Clear,Count,Adress,change,sensor);
	parameter sectores = 359;
	output reg  [8:0] Adress;
	input Clk, Clear,Count,sensor;
	input wire [1:0]change;
	
	
	always @(negedge Clk, negedge Clear)
		if (~Clear )  
				Adress = 0;
		else if (~sensor )  
				Adress = 0;
	else if (change==1)
				Adress = Adress + 1;
				
	else if (Adress >= 359) Adress = 0 ;
		
		

	endmodule
