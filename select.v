module select (data, Clk, Clear,sensor,diseno,rst,rx);

parameter sectores = 360;
parameter dataWidth = 16; 

wire [dataWidth-1: 0 ] conector;
wire [1:0] romactivate;
input Clk, Clear,sensor,rx,diseno,rst;
wire [8:0] romAd;
wire [8:0] adress;
wire alarm;

output [7:0] data;
rxleds RXL(Clk,rx,alarm)
counter1 #(dataWidth,sectores) CNT1(conector, Clk, Clear,alarm,sensor);
comparator #(dataWidth) COM(Clk, Clear,romactivate,conector);
counter3 #(sectores) ADR(Clk, Clear,alarm,romAd,romactivate,sensor);
rom # ( dataWidth ) ROM	(Clk, romAd, data);



endmodule

