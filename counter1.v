module counter1 (Div, Clk, Clear,Count,sensor);
	parameter dataWidth = 16;
	parameter sectores = 360;
	reg  [dataWidth-1: 0 ] Q;
	input Clk, Clear,Count,sensor;
	reg [dataWidth-1: 0 ] D;
	output reg [dataWidth-1: 0 ] Div ;
	reg s;	

	always @(posedge Clk, negedge Clear)
		if (~Clear) begin 
			Q = 0;
			D = 0;
			Div=0;
		
		end else begin
			if (Count && ~sensor && s == 0) begin
				Q=0;
				s=1;
				Div=D;
				D=0;
				if (Div == 0) Div = 1000000;
			end else if (Q < sectores-1) begin 
				Q = Q + 1 ;
				if (sensor) s= 0;
 			end else if (Q == sectores-1) begin 
				D = D + 1;
				Q = 0;
				if (sensor)s=0;			
			end
		end
	endmodule
